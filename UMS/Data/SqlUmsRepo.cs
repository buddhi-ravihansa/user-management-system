using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using UMS.Contexts;
using UMS.Models;

namespace UMS.Data

{
    public class SqlUmsRepo : IUmsRepo
    {
        private readonly UmsContext _context;

        public SqlUmsRepo(UmsContext context)
        {
            _context = context;
        }

        public IEnumerable<User> GetUsers(int faculty)
        {
            return _context.Users
                   .Include(u => u.Department)
                   .Where(u => u.Department.FacultyId == faculty)
                   .ToList();
        }

        public IEnumerable<User> GetUsers(int faculty, int department)
        {
            return _context.Users
                    .Where(u => u.DepartmentId == department)
                    .ToList();
        }

        public IEnumerable<User> GetUsers(int faculty, int department, int role)
        {
            return _context.Users
                    .Where(u => u.DepartmentId == department && u.RoleId == role)
                    .ToList();

        }

        public IEnumerable<User> GetUsers(int faculty, string text)
        {
            return _context.Users
                   .Include(u => u.Department)
                   .Where(u => u.Department.FacultyId == faculty && u.Name.Trim() == text.Trim())
                   .ToList();
        }

        public IEnumerable<User> GetUsers(int faculty, int department, string text)
        {
            return _context.Users
                   .Where(u => u.DepartmentId == department && u.Name.Trim() == text.Trim())
                   .ToList();
        }

        public IEnumerable<User> GetUsers(int faculty, int department, int role, string text)
        {
            return _context.Users
                    .Where(u => u.DepartmentId == department && u.RoleId == role && u.Name.Trim() == text.Trim())
                    .ToList();
        }
    }
}