using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using Xunit.Abstractions;
using UMS.Contexts;
using UMS.Data;
using UMS.Models;

namespace UMS.Tests.Unit
{
    public class SqlUmsRepoTest : AbstractTestWithSqlite
    {
        private readonly ITestOutputHelper _output;
        private readonly SqlUmsRepo _repo;

        public SqlUmsRepoTest(ITestOutputHelper output) : base()
        {
            _output = output;
            _repo = new SqlUmsRepo(_context);
        }

        [Fact]
        public void GetUsers_WithValidFaculty_ReturnsUsersInFaculty()
        {
            // Arrange
            var facultyId = 1;
            var users = new List<User>
            {
                new User { Id = 1, Name = "Buddhi", Email = "bdhd@mail.com", DepartmentId = 1, Department = new Department { Faculty = new Faculty {}, FacultyId = 1}, RoleId = 1, Role = new Role {}},
                new User { Id = 2, Name = "Akila",  Email = "akf@gmail.com", DepartmentId = 1, RoleId = 2, Role = new Role {}}

            };
            _context.Users.AddRange(users);
            _context.SaveChanges();

            // Act
            var result = _repo.GetUsers(facultyId);

            // Assert
            Assert.Equal(2, result.Count());
            Assert.All(result, u => Assert.Equal(facultyId, u.Department.FacultyId));
        }

        [Fact]
        public void GetUsers_WithInValidFaculty_ReturnsUsersInFaculty()
        {
            // Arrange
            var facultyId = 2;
            var users = new List<User>
            {
                new User { Id = 1, Name = "Buddhi", Email = "bdhd@mail.com", DepartmentId = 1, Department = new Department { Faculty = new Faculty {}, FacultyId = 1}, RoleId = 1, Role = new Role {}},
                new User { Id = 2, Name = "Akila",  Email = "akf@gmail.com", DepartmentId = 1, RoleId = 2, Role = new Role {}}

            };
            _context.Users.AddRange(users);
            _context.SaveChanges();

            // Act
            var result = _repo.GetUsers(facultyId);

            // Assert
            Assert.Empty(result);

        }

        [Fact]
        public void GetUsers_WithValidFaculty_ReturnsUsersInFacultyandDepartment()
        {
            // Arrange
            var facultyId = 1;
            var departmentId = 1;
            var users = new List<User>
            {
                new User { Id = 1, Name = "Buddhi", Email = "bdhd@mail.com", DepartmentId = 1, Department = new Department { Faculty = new Faculty {}, FacultyId = 1}, RoleId = 1, Role = new Role {}},
                new User { Id = 2, Name = "Akila",  Email = "akf@gmail.com", DepartmentId = 1, RoleId = 2, Role = new Role {}}

            };
            _context.Users.AddRange(users);
            _context.SaveChanges();

            // Act
            var result = _repo.GetUsers(facultyId, departmentId);

            // Assert
            Assert.Equal(2, result.Count());
            Assert.All(result, u => Assert.Equal(departmentId, u.DepartmentId));
        }

        [Fact]
        public void GetUsers_WithInValidFaculty_ReturnsUsersInFacultyandDepartment()
        {
            // Arrange
            var facultyId = 2;
            var departmentId = 2;
            var users = new List<User>
            {
                new User { Id = 1, Name = "Buddhi", Email = "bdhd@mail.com", DepartmentId = 1, Department = new Department { Faculty = new Faculty {}, FacultyId = 1}, RoleId = 1, Role = new Role {}},
                new User { Id = 2, Name = "Akila",  Email = "akf@gmail.com", DepartmentId = 1, RoleId = 2, Role = new Role {}}

            };
            _context.Users.AddRange(users);
            _context.SaveChanges();

            // Act
            var result = _repo.GetUsers(facultyId, departmentId);

            // Assert
            Assert.Empty(result);
        }

        [Fact]
        public void GetUsers_WithValidFaculty_ReturnsUsersInFacultyandDepartmentandRole()
        {
            // Arrange
            var facultyId = 1;
            int departmentId = 1;
            int roleId = 1;
            var users = new List<User>
            {
                new User { Id = 1, Name = "Buddhi", Email = "bdhd@mail.com", DepartmentId = 1, Department = new Department { Faculty = new Faculty {}, FacultyId = 1}, RoleId = 1, Role = new Role {}},
                new User { Id = 2, Name = "Akila",  Email = "akf@gmail.com", DepartmentId = 1, RoleId = 2, Role = new Role {}}

            };
            _context.Users.AddRange(users);
            _context.SaveChanges();

            // Act
            var result = _repo.GetUsers(facultyId, departmentId, roleId);

            // Assert
            Assert.Single(result);
            Assert.All(result, u =>
                   {
                       Assert.Equal(facultyId, u.Department.FacultyId);
                       Assert.Equal(departmentId, u.DepartmentId);
                       Assert.Equal(roleId, u.RoleId);
                   });
        }

        [Fact]
        public void GetUsers_WithInValidFaculty_ReturnsUsersInFacultyandDepartmentandRole()
        {
            // Arrange
            var facultyId = 2;
            int departmentId = 1;
            int roleId = 3;
            var users = new List<User>
            {
                new User { Id = 1, Name = "Buddhi", Email = "bdhd@mail.com", DepartmentId = 1, Department = new Department { Faculty = new Faculty {}, FacultyId = 1}, RoleId = 1, Role = new Role {}},
                new User { Id = 2, Name = "Akila",  Email = "akf@gmail.com", DepartmentId = 1, RoleId = 2, Role = new Role {}}

            };
            _context.Users.AddRange(users);
            _context.SaveChanges();

            // Act
            var result = _repo.GetUsers(facultyId, departmentId, roleId);

            // Assert
            Assert.Empty(result);

        }

        [Fact]
        public void GetUsers_WithInValidFaculty_ReturnsUsersInFacultyandDepartmentandRole2()
        {
            // Arrange
            var facultyId = 1;
            int departmentId = 3;
            int roleId = 3;
            var users = new List<User>
            {
                new User { Id = 1, Name = "Buddhi", Email = "bdhd@mail.com", DepartmentId = 1, Department = new Department { Faculty = new Faculty {}, FacultyId = 1}, RoleId = 1, Role = new Role {}},
                new User { Id = 2, Name = "Akila",  Email = "akf@gmail.com", DepartmentId = 1, RoleId = 2, Role = new Role {}}

            };
            _context.Users.AddRange(users);
            _context.SaveChanges();

            // Act
            var result = _repo.GetUsers(facultyId, departmentId, roleId);

            // Assert
            Assert.Empty(result);

        }

        [Fact]
        public void GetUsers_WithValidFaculty_ReturnsUsersInFacultyandName()
        {
            // Arrange
            var facultyId = 1;
            string text = "Buddhi";
            var users = new List<User>
            {
                new User { Id = 1, Name = "Buddhi", Email = "bdhd@mail.com", DepartmentId = 1, Department = new Department { Faculty = new Faculty {}, FacultyId = 1}, RoleId = 1, Role = new Role {}},
                new User { Id = 2, Name = "Akila",  Email = "akf@gmail.com", DepartmentId = 1, RoleId = 2, Role = new Role {}}

            };
            _context.Users.AddRange(users);
            _context.SaveChanges();

            // Act
            var result = _repo.GetUsers(facultyId, text);

            // Assert
            Assert.Single(result);
            Assert.All(result, u =>
                   {
                       Assert.Equal(facultyId, u.Department.FacultyId);
                       Assert.Equal(text, u.Name);
                   });
        }

        [Fact]
        public void GetUsers_WithInValidFaculty_ReturnsUsersInFacultyandName()
        {
            // Arrange
            var facultyId = 2;
            string text = "jdjdnjd";
            var users = new List<User>
            {
                new User { Id = 1, Name = "Buddhi", Email = "bdhd@mail.com", DepartmentId = 1, Department = new Department { Faculty = new Faculty {}, FacultyId = 1}, RoleId = 1, Role = new Role {}},
                new User { Id = 2, Name = "Akila",  Email = "akf@gmail.com", DepartmentId = 1, RoleId = 2, Role = new Role {}}

            };
            _context.Users.AddRange(users);
            _context.SaveChanges();

            // Act
            var result = _repo.GetUsers(facultyId, text);

            // Assert
            Assert.Empty(result);

        }

        [Fact]
        public void GetUsers_WithValidFaculty_ReturnsUsersInFacultyandDepartmentandtext()
        {
            // Arrange
            var facultyId = 1;
            var departmentId = 1;
            string text = "Buddhi";
            var users = new List<User>
            {
                new User { Id = 1, Name = "Buddhi", Email = "bdhd@mail.com", DepartmentId = 1, Department = new Department { Faculty = new Faculty {}, FacultyId = 1}, RoleId = 1, Role = new Role {}},
                new User { Id = 2, Name = "Akila",  Email = "akf@gmail.com", DepartmentId = 1, RoleId = 2, Role = new Role {}}

            };
            _context.Users.AddRange(users);
            _context.SaveChanges();

            // Act
            var result = _repo.GetUsers(facultyId, departmentId, text);

            // Assert
            Assert.Single(result);
            Assert.All(result, u =>
                   {
                       Assert.Equal(facultyId, u.Department.FacultyId);
                       Assert.Equal(departmentId, u.DepartmentId);
                       Assert.Equal(text, u.Name);
                   });
        }

        [Fact]
        public void GetUsers_WithInValidFaculty_ReturnsUsersInFacultyandDepartmentandtext()
        {
            // Arrange
            var facultyId = 2;
            var departmentId = 2;
            string text = "bcjnc";
            var users = new List<User>
            {
                new User { Id = 1, Name = "Buddhi", Email = "bdhd@mail.com", DepartmentId = 1, Department = new Department { Faculty = new Faculty {}, FacultyId = 1}, RoleId = 1, Role = new Role {}},
                new User { Id = 2, Name = "Akila",  Email = "akf@gmail.com", DepartmentId = 1, RoleId = 2, Role = new Role {}}

            };
            _context.Users.AddRange(users);
            _context.SaveChanges();

            // Act
            var result = _repo.GetUsers(facultyId, departmentId, text);

            // Assert
            Assert.Empty(result);
        }

        [Fact]
        public void GetUsers_WithValidFaculty_ReturnsUsersInFacultyandDepartmentandRoleandtext()
        {
            // Arrange
            var facultyId = 1;
            int departmentId = 1;
            int roleId = 2;
            string text = "Akila";
            var users = new List<User>
            {
                new User { Id = 1, Name = "Buddhi", Email = "bdhd@mail.com", DepartmentId = 1, Department = new Department { Faculty = new Faculty {}, FacultyId = 1}, RoleId = 1, Role = new Role {}},
                new User { Id = 2, Name = "Akila",  Email = "akf@gmail.com", DepartmentId = 1, RoleId = 2, Role = new Role {}}

            };
            _context.Users.AddRange(users);
            _context.SaveChanges();

            // Act
            var result = _repo.GetUsers(facultyId, departmentId, roleId, text);

            // Assert
            Assert.Single(result);
            Assert.All(result, u =>
                   {
                       Assert.Equal(facultyId, u.Department.FacultyId);
                       Assert.Equal(departmentId, u.DepartmentId);
                       Assert.Equal(roleId, u.RoleId);
                       Assert.Equal(text, u.Name);
                   });
        }

        [Fact]
        public void GetUsers_WithInValidFaculty_ReturnsUsersInFacultyandDepartmentandRoleandText()
        {
            // Arrange
            var facultyId = 2;
            int departmentId = 1;
            int roleId = 3;
            string text = "mnmdee";
            var users = new List<User>
            {
                new User { Id = 1, Name = "Buddhi", Email = "bdhd@mail.com", DepartmentId = 1, Department = new Department { Faculty = new Faculty {}, FacultyId = 1}, RoleId = 1, Role = new Role {}},
                new User { Id = 2, Name = "Akila",  Email = "akf@gmail.com", DepartmentId = 1, RoleId = 2, Role = new Role {}}

            };
            _context.Users.AddRange(users);
            _context.SaveChanges();

            // Act
            var result = _repo.GetUsers(facultyId, departmentId, roleId, text);

            // Assert
            Assert.Empty(result);

        }

    }
}
