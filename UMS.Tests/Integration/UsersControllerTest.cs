using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using UMS.Controllers;
using UMS.Contexts;
using UMS.Dtos;
using UMS.Models;
using UMS.Data;
using UMS.Tests;
using UMS.Profiles;
using Xunit;

namespace Tests
{
    public class UsersControllerTests : AbstractTestWithSqlite
    {
        private readonly UsersController _controller;

        public UsersControllerTests()
        {
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new UserProfiles());
            });

            var mapper = mapperConfig.CreateMapper();
            var repository = new SqlUmsRepo(_context);
            _controller = new UsersController(repository, mapper);
        }

        // Test case for GET api/users
        [Fact]
        public async Task GetUsers_WithValidfourParameters_ReturnsOkResult()
        {
            // Arrange
            int faculty = 1;
            int department = 1;
            int role = 1;
            string text = "Buddhi";

            _context.Users.AddRange(
                 new User { Id = 1, Name = "Buddhi", Email = "bdhd@mail.com", DepartmentId = 1, Department = new Department { Faculty = new Faculty { }, FacultyId = 1 }, RoleId = 1, Role = new Role { } },
                 new User { Id = 2, Name = "Akila", Email = "akf@gmail.com", DepartmentId = 1, RoleId = 2, Role = new Role { } }
            );
            _context.SaveChanges();

            // Act
            var result = _controller.GetUsers(faculty, department, role, text);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var userDtos = Assert.IsAssignableFrom<IEnumerable<UserReadDto>>(okResult.Value);
            Assert.Single(userDtos);
        }

        [Fact]
        public async Task GetUsers_WithInValidfourParameters_ReturnsOkResult()
        {
            // Arrange
            int faculty = 1;
            int department = 1;
            int role = 1;
            string text = "Nishan";

            _context.Users.AddRange(
                 new User { Id = 1, Name = "Buddhi", Email = "bdhd@mail.com", DepartmentId = 1, Department = new Department { Faculty = new Faculty { }, FacultyId = 1 }, RoleId = 1, Role = new Role { } },
                 new User { Id = 2, Name = "Akila", Email = "akf@gmail.com", DepartmentId = 1, RoleId = 2, Role = new Role { } }
            );
            _context.SaveChanges();

            // Act
            var result = _controller.GetUsers(faculty, department, role, text);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var userDtos = Assert.IsAssignableFrom<IEnumerable<UserReadDto>>(okResult.Value);
            Assert.Equal(0, userDtos.Count());
        }

        [Fact]
        public async Task GetUsers_WithValidthreeParameters_ReturnsOkResult()
        {
            // Arrange
            int faculty = 1;
            int department = 1;
            int role = 1;

            _context.Users.AddRange(
                 new User { Id = 1, Name = "Buddhi", Email = "bdhd@mail.com", DepartmentId = 1, Department = new Department { Faculty = new Faculty { }, FacultyId = 1 }, RoleId = 1, Role = new Role { } },
                 new User { Id = 2, Name = "Akila", Email = "akf@gmail.com", DepartmentId = 1, RoleId = 2, Role = new Role { } }
            );
            _context.SaveChanges();

            // Act
            var result = _controller.GetUsers(faculty, department, role, null);


            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var userDtos = Assert.IsAssignableFrom<IEnumerable<UserReadDto>>(okResult.Value);
            Assert.Single(userDtos);
        }

        [Fact]
        public async Task GetUsers_WithValidthreeincludingtextParameters_ReturnsOkResult()
        {
            // Arrange
            int faculty = 1;
            int department = 1;
            string text = "Buddhi";

            _context.Users.AddRange(
                 new User { Id = 1, Name = "Buddhi", Email = "bdhd@mail.com", DepartmentId = 1, Department = new Department { Faculty = new Faculty { }, FacultyId = 1 }, RoleId = 1, Role = new Role { } },
                 new User { Id = 2, Name = "Akila", Email = "akf@gmail.com", DepartmentId = 1, RoleId = 2, Role = new Role { } }
            );
            _context.SaveChanges();

            // Act
            var result = _controller.GetUsers(faculty, department, 0, text);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var userDtos = Assert.IsAssignableFrom<IEnumerable<UserReadDto>>(okResult.Value);
            Assert.Single(userDtos);
        }

        [Fact]
        public async Task GetUsers_WithValidwoParameters_ReturnsOkResult()
        {
            // Arrange
            int faculty = 1;
            int department = 1;

            _context.Users.AddRange(
                 new User { Id = 1, Name = "Buddhi", Email = "bdhd@mail.com", DepartmentId = 1, Department = new Department { Faculty = new Faculty { }, FacultyId = 1 }, RoleId = 1, Role = new Role { } },
                 new User { Id = 2, Name = "Akila", Email = "akf@gmail.com", DepartmentId = 1, RoleId = 2, Role = new Role { } }
            );
            _context.SaveChanges();

            // Act
            var result = _controller.GetUsers(faculty, department, 0, null);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var userDtos = Assert.IsAssignableFrom<IEnumerable<UserReadDto>>(okResult.Value);
            Assert.Equal(2, userDtos.Count());
        }

        [Fact]
        public async Task GetUsers_WithinValidfourParameters_ReturnsOkResult()
        {
            // Arrange
            int faculty = 1;
            int department = 30;

            _context.Users.AddRange(
                 new User { Id = 1, Name = "Buddhi", Email = "bdhd@mail.com", DepartmentId = 1, Department = new Department { Faculty = new Faculty { }, FacultyId = 1 }, RoleId = 1, Role = new Role { } },
                 new User { Id = 2, Name = "Akila", Email = "akf@gmail.com", DepartmentId = 1, RoleId = 2, Role = new Role { } }
            );
            _context.SaveChanges();

            // Act
            var result = _controller.GetUsers(faculty, department, 0, null);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var userDtos = Assert.IsAssignableFrom<IEnumerable<UserReadDto>>(okResult.Value);
            Assert.Equal(0, userDtos.Count());
        }

        [Fact]
        public async Task GetUsers_WithValidtwoincludingtextParameters_ReturnsOkResult()
        {
            // Arrange
            int faculty = 1;
            string text = "Buddhi";

            _context.Users.AddRange(
                 new User { Id = 1, Name = "Buddhi", Email = "bdhd@mail.com", DepartmentId = 1, Department = new Department { Faculty = new Faculty { }, FacultyId = 1 }, RoleId = 1, Role = new Role { } },
                 new User { Id = 2, Name = "Akila", Email = "akf@gmail.com", DepartmentId = 1, RoleId = 2, Role = new Role { } }
            );
            _context.SaveChanges();

            // Act
            var result = _controller.GetUsers(faculty, 0, 0, text);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var userDtos = Assert.IsAssignableFrom<IEnumerable<UserReadDto>>(okResult.Value);
            Assert.Single(userDtos);
        }

        [Fact]
        public async Task GetUsers_WithValidfacultyParameters_ReturnsOkResult()
        {
            // Arrange
            int faculty = 1;

            _context.Users.AddRange(
                 new User { Id = 1, Name = "Buddhi", Email = "bdhd@mail.com", DepartmentId = 1, Department = new Department { Faculty = new Faculty { }, FacultyId = 1 }, RoleId = 1, Role = new Role { } },
                 new User { Id = 2, Name = "Akila", Email = "akf@gmail.com", DepartmentId = 1, RoleId = 2, Role = new Role { } }
            );
            _context.SaveChanges();

            // Act
            var result = _controller.GetUsers(faculty, 0, 0, null);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            var userDtos = Assert.IsAssignableFrom<IEnumerable<UserReadDto>>(okResult.Value);
            Assert.Equal(2, userDtos.Count());
        }

        // Test case for GET api/users with missing parameters
        [Fact]
        public async Task GetUsers_WithMissingParameters_ReturnsBadRequest()
        {
            // Arrange

            // Act
            var result = _controller.GetUsers(0, 0, 0, null);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result.Result);
        }
    }
}
