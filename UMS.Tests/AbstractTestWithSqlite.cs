using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using UMS.Contexts;

namespace UMS.Tests
{
    public abstract class AbstractTestWithSqlite : IDisposable
    {
        protected readonly UmsContext _context;
        private readonly SqliteConnection _connection;

        protected AbstractTestWithSqlite()
        {
            _connection = new SqliteConnection("DataSource=:memory:");
            _connection.Open();

            var options = new DbContextOptionsBuilder<UmsContext>()
                .UseSqlite(_connection)
                .Options;

            _context = new UmsContext(options);
            _context.Database.EnsureCreated();
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
            _connection.Close();
            _connection.Dispose();
        }
    }
}
